class AdministrationController < ApplicationController
  before_action :index, :verify_authorization
  before_action :index, :authenticate_user!

  def index
    @last_registrations = User.last_registrations(10)
    @last_purchases = Purchase.order(created_at: :desc).limit(10).includes(:user, :products).map { |p| {user_email:  p.user.email, user_id:p.user.id, amount:p.amount,purchase_id: p.id, purchase_status:p.status_name} }
    @last_payments = Purchase.paid.order(created_at: :desc).limit(10).includes(:user, :products).map { |p| {user_email:  p.user.email, user_id:p.user.id, amount:p.amount,purchase_id: p.id, purchase_status:p.status_name} }
    @recently_added_products = Product.with_attached_image.order(created_at: :desc).limit(10)
  end
end
