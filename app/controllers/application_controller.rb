class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  helper_method :admin?

  protected

  def verify_authorization
    unless admin?
      redirect_to root_path
    end
  end

  def admin?
    user_signed_in? && current_user.role == 1
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:email, :password, :password_confirmation, :role) }
  end
end
