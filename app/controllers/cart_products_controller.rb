class CartProductsController < ApplicationController

  def create
    chosen_product = Product.find(cart_product_params[:product_id])
    user_cart = Cart.find(cart_product_params[:cart_id])

    if user_cart.products.include?(chosen_product)
      @cart_product = CartProduct.find_by(cart_id: user_cart.id,
                                          product_id: chosen_product.id)
      @cart_product.quantity += 1
      respond_to do |format|
        if @cart_product.save
          format.html { redirect_to root_path, notice: 'Quantity increased' }
        else
          format.html { render :new, notice: 'Error' }
        end
      end
    else
      @cart_product = CartProduct.new(cart_product_params)
      respond_to do |format|
        if @cart_product.save
          format.html { redirect_to root_path, notice: 'Product successfully added to cart' }
        else
          format.html { render :new, notice: 'Error' }
        end
      end
    end
  end

  # this needs AJAX
  def increment_product_quantity
    cart_product = CartProduct.find(params[:id])
    cart_product.quantity += 1
    respond_to do |format|
      if cart_product.save
        flash.now[:notice] = 'Product removed'
        format.html { redirect_back fallback_location: root_path }
      else
        flash.now[:alert] = 'Error, couldn\'t decrement product\'s quantity'
        format.html { redirect_back fallback_location: root_path }
      end
    end
  end

  def decrement_product_quantity
    cart_product = CartProduct.find(params[:id])
    if cart_product.quantity == 1
      CartProduct.destroy(cart_product.id)
      redirect_back fallback_location: root_path
    else
      cart_product.quantity -= 1
      respond_to do |format|
        if cart_product.save
          flash.now[:notice] = 'Product removed'
          format.html { redirect_back fallback_location: root_path }
        else
          flash.now[:alert] = 'Error, couldn\'t decrement product\'s quantity'
          format.html { redirect_back fallback_location: root_path }
        end
      end
    end
  end

  private

  def cart_product_params
    params.require(:cart_product).permit(:cart_id, :product_id, :quantity)
  end
end
