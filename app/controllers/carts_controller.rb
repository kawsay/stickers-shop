class CartsController < ApplicationController
  def show
    @cart = Cart.user_cart_with_products(current_user)
  end
end
