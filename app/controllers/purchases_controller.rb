class PurchasesController < ApplicationController
  before_action :set_purchase, only: :show

  def show
  end

  private

  def set_purchase
    @purchase = Purchase.find(params[:id])
  end
end
