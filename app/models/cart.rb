class Cart < ApplicationRecord
  belongs_to :user
  has_many :cart_products
  has_many :products, through: :cart_products

  validates :user_id, presence: true

  scope :user_cart_with_products, -> (user) { includes(:cart_products, :products).find_by(user_id: user) }

  def amount
    self.cart_products.inject(0) { |sum, cp| sum + cp.quantity * cp.product.price }
  end
end
