class Product < ApplicationRecord
  PRODUCT_STATUS = {
    active: 1,
    deleted: 2,
    na: 3,
    out_of_stock: 4
  }.freeze

  belongs_to :supplier, class_name: 'User'
  has_one :product_category, dependent: :destroy
  has_one :category, through: :product_category
  has_many :product_tags, dependent: :destroy
  has_many :tags, through: :product_tags
  has_many :cart_products, dependent: :destroy
  has_many :carts, through: :cart_products
  has_many :purchase_products, dependent: :destroy
  has_many :purchases, through: :purchase_products
  has_one_attached :image

  validates :name, :description, :price, :quantity, :status, presence: true
  validates :price, :quantity, numericality: true
  validates_inclusion_of :status, in: PRODUCT_STATUS.values
  validates :supplier_id, presence: true

  def truncated_description
    self.description.truncate(50, separator: ' ', omission: '...')
  end

  def card_image
    resize_image(400, 200)
  end
  def gallery_image
    resize_image(600, 400)
  end
  def gallery_item
    resize_image(100, 50)
  end
  def thumbnail
    resize_image(50, 50)
  end

  private

  def resize_image(x, y)
    self.image.variant(resize_to_limit: [x, y]).processed
  end
end
