class ProductTag < ApplicationRecord
  belongs_to :product
  belongs_to :tag

  validates :product_id, :tag_id, presence: true
end
