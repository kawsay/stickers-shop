class Purchase < ApplicationRecord
  PURCHASE_STATUS = {
    delivered: 1,
    paid: 2,
    canceled: 3,
    na: 4
  }.freeze

  belongs_to :user
  has_many :purchase_products
  has_many :products, through: :purchase_products
  
  validates :user_id, :status, presence: true
  validates :status, numericality: true
  validates_inclusion_of :status, in: PURCHASE_STATUS.values
  
  # Create scopes related to status (:delivered, :paid, ...)
  PURCHASE_STATUS.each do |s|
    scope s[0], -> { where(status: s[1]) }
  end
  
  def status_name
    PURCHASE_STATUS.key(self.status).to_s.upcase
  end

  def amount
    self.products.sum(&:price)
  end
end
