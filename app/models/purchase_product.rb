class PurchaseProduct < ApplicationRecord
  belongs_to :purchase
  belongs_to :product

  validates :product_id, :purchase_id, presence: true
end
