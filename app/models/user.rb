class User < ApplicationRecord
  USER_ROLE = {
    admin: 1,
    client: 2,
    supplier: 3
  }.freeze

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :validatable

  has_many :purchases
  has_one :cart, dependent: :destroy

  after_save :set_user_cart

  validates :email, :role, presence: true
  validates :email, format: URI::MailTo::EMAIL_REGEXP
  validates_inclusion_of :role, in: USER_ROLE.values

  scope :suppliers, -> { where(role: 3)}
  scope :clients, -> { where(role:2) }
  scope :last_registrations, -> (n) { order(created_at: :desc).limit(n) }
  scope :identity, -> { select(:id, :email, :role) }

  def role_name
    USER_ROLE.key(self.role).to_s.capitalize
  end

  private

  def set_user_cart
    Cart.create!(user_id: self.id)
  end
end
