Rails.application.routes.draw do
  get 'administration/index'
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: "products#index"

  resources :products, only: [:index, :new, :create, :show, :edit, :update, :destroy]
  resources :users, only: [:show, :update]
  resources :carts, only: [:show]
  resources :purchases, only: [:show]
  resources :cart_products, only: [:create]
  # TODO: learn to write routes
      post 'cart_products/:id/increment_product_quantity/', to: 'cart_products#increment_product_quantity', as: 'increment_product_quantity'
      post 'cart_products/:id/decrement_product_quantity/', to: 'cart_products#decrement_product_quantity', as: 'decrement_product_quantity'
end
