class AddIndexes < ActiveRecord::Migration[6.0]
  def up
    # Remove unecessaries tables
#    drop_table :user_roles
#    drop_table :statuses
#    drop_table :roles
#    drop_table :product_statuses

    # Add roles and statuses as integers
    add_column :products, :status, :integer
    add_column :users, :role, :integer

    ### Indexes for queries involving 2 fields
    #
    # Join tables
    add_index :purchase_products, [:purchase_id, :product_id]
    # Purchase.last.products
    add_index :cart_products, [:cart_id, :product_id]
    # Cart.last.products
    add_index :product_categories, [:category_id, :product_id]
    # Product.last.category
    add_index :product_tags, [:tag_id, :product_id]
    add_index :purchases, [:status, :user_id]
    # Might be useful for having the user' history
    # SELECT * FROM purchases WHERE 'status = 1' AND 'user_id = 23'


    ### Indexes for specific fields
    #
    # Find user when he logs in. heroku rake db:migrates says it already exists. Devise default ?
#    add_index :users, :email
    # Filter supplier/clients, look up if user is admin
    add_index :users, :role
    # List products with specific status
    add_index :products, :status
    #add_index :carts, :id
    add_index :purchases, :id
    add_index :purchases, :status
#    add_index :purchases, :user_id
#    add_index :carts, :user_id
  end

  def down
    # Remove unecessaries tables
    create_table :user_roles
    create_table :statuses
    create_table :roles
    create_table :product_statuses

    # Add roles and statuses as integers
    remove_column :products, :status, :integer
    remove_column :users, :role, :integer

    ### Indexes for queries involving 2 fields
    #
    # Join tables
    remove_index :purchase_products, [:purchase_id, :product_id]
    remove_index :cart_products, [:cart_id, :product_id]
    remove_index :product_categories, [:category_id, :product_id]
    remove_index :product_tags, [:tag_id, :product_id]
    #
    # Might be useful for having the user' history
    # SELECT * FROM purchases WHERE 'status = 1' AND 'user_id = 23'
    remove_index :purchases, [:status, :user_id]


    ### Indexes for specific fields
    #
    remove_index :users, :email
    #remove_index :users, :role
    remove_index :purchases, :id
    remove_index :purchases, :status
    remove_index :purchases, :user_id
    remove_index :carts, :user_id
    remove_index :carts, :id

  end
end
