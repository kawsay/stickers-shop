class CreateUserNotifications < ActiveRecord::Migration[6.0]
  def change
    create_table :user_notifications do |t|
      t.references :users, null: false, foreign_key: true
      t.text :body

      t.timestamps
    end

    # Automatically added by Rails
    # add_index :user_notifications, :users_id
  end
end
