class ChangePurchaseStatusToBeInteger < ActiveRecord::Migration[6.0]
  def change
    change_column :purchases, :status, 'integer USING CAST(status AS integer)'
  end
end
