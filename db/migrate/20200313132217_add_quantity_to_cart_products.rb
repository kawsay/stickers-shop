class AddQuantityToCartProducts < ActiveRecord::Migration[6.0]
  def change
    add_column :cart_products, :quantity, :integer, default: 1
    add_column :purchase_products, :quantity, :integer, default: 1
  end
end
