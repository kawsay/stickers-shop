class AddClientIdToProduct < ActiveRecord::Migration[6.0]
  def change
    add_reference :products, :supplier, index: true, foreign_key: { to_table: :users }
  end
end
