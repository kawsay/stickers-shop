# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

puts 'INFO: Generating thousands of records may take few minutes'

print 'Cleaning tables... '
[ProductCategory, ProductTag, CartProduct, PurchaseProduct,
 Category, Tag, Product, Cart, Purchase, User]
  .each { |table| table.all.destroy_all }
puts '✔'

print 'Creating Categories... '
10.times do
  Category.new(
    name: Faker::Ancient.god
  ).save(validate: false)
end
puts '✔'

print 'Creating Tags... '
20.times do
  Tag.new(
    name: Faker::FunnyName.name
  ).save(validate: false)
end
puts '✔'


print 'Creating Users... '
500.times do
  User.new(
    email: Faker::Internet.email,
    password: 'foobar',
    role: rand(1..3)
  ).save(validate: false)
end
puts '✔'

print 'Creating Products... '
suppliers = User.suppliers
img_paths = Dir.glob(Rails.root.join('resources', 'sticker_images', '*'))
2000.times do
  img = img_paths.sample
  Product.create!(
    name: Faker::Artist.name,
    description: Faker::Lorem.paragraph,
    price: rand(100),
    status: rand(1..4),
    quantity: rand(100),
    supplier: suppliers.sample
  ).image.attach(io: File.open(img),
                 filename: img.split('/').last)
end
puts '✔'
puts Product.all.size

print 'Creating Product\'s join tables... '
Product.all.each do |p|
  ProductCategory.new(
    product_id: p.id,
    category_id: Category.all.sample.id
  ).save(validate: false)

  ProductTag.new(
    product_id: p.id,
    tag_id: Tag.all.sample.id
  ).save(validate: false)
end
puts '✔'



print 'Creating user\' purchases/carts and roles... '
User.all.each do |u|
  Purchase.new(user_id: u.id,
               status: rand(1..4)).save(validate: false)
  Cart.new(user_id: u.id).save(validate: false)
end
puts '✔'

print 'Creating purchases\' and carts\' products... '
5000.times do
  CartProduct.new(
    cart_id: Cart.all.sample.id,
    product_id: Product.all.sample.id,
    quantity: rand(1..10)
  ).save(validate: false)
  PurchaseProduct.new(
    purchase_id: Purchase.all.sample.id,
    product_id: Product.all.sample.id,
    quantity: rand(1..10)
  ).save(validate: false)
end 
puts '✔'

print 'Creating User: foo@bar.com / foobar ... '
User.new(email: 'foo@bar.com', password: 'foobar', role: 1).save(validate: false)
Purchase.new(user_id: User.last.id, status: 4).save(validate: false)
Cart.new(user_id: User.last.id).save(validate: false)
puts '✔'


